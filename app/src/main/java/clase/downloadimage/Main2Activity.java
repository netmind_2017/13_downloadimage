package clase.downloadimage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.URL;

public class Main2Activity extends AppCompatActivity {

    private RelativeLayout relative;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        relative = (RelativeLayout) findViewById(R.id.activity_main2);

        new DownloadImage().execute("https://s-media-cache-ak0.pinimg.com/736x/e1/41/6b/e1416b7e8d87282363b1f4b8e6f3a51b.jpg");

    }

    public class DownloadImage extends AsyncTask<String, Integer, Drawable> {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected Drawable doInBackground(String... arg) {

            URL url;
            BufferedOutputStream out;
            InputStream in;
            BufferedInputStream buf;

            try {
                url = new URL(arg[0]);
                in = url.openStream();

                buf = new BufferedInputStream(in);

                // Convert the BufferedInputStream to a Bitmap
                Bitmap bMap = BitmapFactory.decodeStream(buf);
                if (in != null) {
                    in.close();
                }
                if (buf != null) {
                    buf.close();
                }

                return new BitmapDrawable(bMap);

            } catch (Exception e) {
                Log.e("ERROR", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Drawable image)
        {
            relative.setBackground(image);

        }

    }
}
